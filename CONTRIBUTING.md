# Contributing

Ce document présente le processus de travail et l'environnement de développement

## Processus

Créer une nouvelle branche pour chaque nouvelle feature ou bug à corriger
```
git checkout -b feature-XXX
```

Une fois les modifications effectuées, ajouter les nouveaux fichiers au tracking git
```
git add fichier1 fichier2 ...
```

Puis commiter les mises à jour
```
git commit -m "feature-XXX : nouvelles mises à jour"
```

## Environnement de développement
