
const gulp = require('gulp');
const sass = require('gulp-sass');
const rename = require('gulp-rename');

sass.compiler = require('sass');

const processSass = () => {
    return gulp
        .src('./src/sass/main.sass')
        .pipe(sass())
        .pipe(rename('style.css'))
        .pipe(gulp.dest('./build'));
}

gulp.task('sass', processSass);
gulp.task('sass:watch', () => {
    gulp.watch('./src/sass/*.sass', processSass)
});
