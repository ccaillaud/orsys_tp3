
const gulp = require('gulp');
const rename = require('gulp-rename')

gulp.task('bundle-css-libs', () => {
    return gulp
        .src('./node_modules/bootstrap/dist/css/bootstrap.min.css')
        .pipe(rename('libs.min.css'))
        .pipe(gulp.dest('./build'));
});
