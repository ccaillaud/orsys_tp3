
const gulp = require('gulp');
const processHtml = require('gulp-processhtml');

gulp.task('bundle-html', () => {
    return gulp
        .src('./index.html')
        .pipe(processHtml())
        .pipe(gulp.dest('./build'));
});
