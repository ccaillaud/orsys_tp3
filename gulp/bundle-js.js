
const gulp = require('gulp');
const webpack = require('webpack-stream');
const concat = require('gulp-concat');
const minify = require('gulp-minify');

gulp.task('bundle-js', () => {
    return gulp
        .src([ './src/*.js', './src/view/*.js', './src/data/*.js' ])
        .pipe(webpack({
            mode: 'production'
        }))
        .pipe(concat('main.js'))
        .pipe(minify({
            ext: '.min.js'
        }))
        .pipe(gulp.dest('./build'))
});
