
const gulp = require('gulp');
const clean = require('gulp-clean-css');
const rename = require('gulp-rename');

gulp.task('bundle-css', () => {
    return gulp
        .src('./build/style.css')
        .pipe(clean())
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('./build'));
});
