
const gulp = require('gulp');
const concat = require('gulp-concat');

gulp.task('bundle-js-libs', () => {
    return gulp.src([ './node_modules/jquery/dist/jquery.min.js', './node_modules/bootstrap/dist/js/bootstrap.min.js' ])
        .pipe(concat('libs.min.js'))
        .pipe(gulp.dest('./build'));
});
