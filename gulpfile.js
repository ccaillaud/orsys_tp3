

const gulp = require('gulp');
const requireDir = require('require-dir');

requireDir('./gulp');

gulp.task('bundle', gulp.series([
    'sass',
    'bundle-css',
    'bundle-js',
    'bundle-js-libs',
    'bundle-css-libs',
    'bundle-html'
]));