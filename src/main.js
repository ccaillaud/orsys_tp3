
import users from './data/users.js';
import todos from './data/todos.js';
import { App } from './app.js';

window.onload = () => {
    new App(users, todos).render();
};
