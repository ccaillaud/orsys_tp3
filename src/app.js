
import { Todo } from './views/todo.js';
import { TodoList } from './views/todolist.js';

export class App {
	constructor(users, todos) {
		this.todoLists = users.map(
			user => new TodoList(
				user,
				todos
					.filter(todo => todo.userId === user.id)
					.map(todo => new Todo(todo))
			)
		);
	}

	render() {
		$("#content").append(
			this.todoLists.map(todoList => todoList.render())
		);
	}
}
