
export class TodoList {

	constructor(user, todos) {
		this.user = user;
		this.todos = todos;
	}

	render() {

		let html = [];

		html.push(`<div class="accordion">`);
		html.push(`<div class="card">`);
		html.push(`<div class="card-header" data-toggle="collapse" data-target="#toggle-${this.user.id}" aria-expanded="false" aria-controls="toggle-${this.user.id}">`);
		html.push(`<h2>${this.user.name}</h2>`);
		html.push(`<p class="status"></p>`);
		html.push(`<div class="progress">`);
		html.push(`<div class="progress-bar" role="progressbar" aria-valuenow="${this.completed()}" aria-valuemin="0" aria-valuemax="${this.total()}">`);
		html.push(`</div>`);
		html.push(`</div>`);
		html.push(`</div>`);
		html.push(`<ul id="toggle-${this.user.id}" class="collapse"></ul>`);
		html.push(`</div>`);
		html.push(`</div>`);

		this.$el = $(html.join(''));
		let $subEls = this.todos.map(todo => todo.render());

		$subEls.forEach($el => {
			$el.on('todo:changed', () => {
				this.updateCompleted();
			});
		});

		this.$el.find('ul').append($subEls);
		this.updateCompleted();
		return this.$el;
	}

	updateCompleted() {

		let isCompleted = this.completed() === this.total();

		this.$el
			.find('.status')
			.text(`${this.completed()}/${this.total()}`)
			.toggleClass('completed', isCompleted)
			.toggleClass('not-completed', !isCompleted);

		this.$el
			.find('.progress-bar')
			.attr('aria-valuenow', this.completed())
			.css('width', this.completed() * 100 / this.total() + '%')
			.toggleClass('bg-success', isCompleted)
			.toggleClass('bg-danger', !isCompleted);
	}

	completed() {
		return this.todos.filter(todo => todo.completed).length;
	}

	total() {
		return this.todos.length;
	}
}