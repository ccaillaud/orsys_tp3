
export class Todo {

	constructor({ title, completed }) {
		this.title = title;
		this.completed = completed;
	}

	render() {
		this.$el = $(`<li><label><input type="checkbox" ${this.completed ? 'checked' : ''}/> ${this.title}</label></li>`);

		this.$el.find('input').on('change', (e) => {
			this.completed = e.target.checked;
			this.$el.trigger('todo:changed');
		});

		return this.$el;
	}
}
